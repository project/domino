# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.4.3]
- Remove unnecessary error log for mailslurp feature.

## [3.4.2]
- #3443369: Streamline Mailslurp testing.

## [3.4.1]
- #3398032: Database error when domino module is included in core.extensions.yml during site install with config

## [3.4.0]
- #3372456: Allow replacing the recipients of the emails by settings a cookie for testing

## [3.3.2]
- #3338569: Ensure all entities with an owner gracefully fallback to anonymous owner after truncating all user tables

## [3.3.1]
- #3338511: Ensure anonymous and super-admin users are always exists in the system

## [3.3.0]
- #3338150: Throws error when user with uid 1 does not exist
- #3338160: Provide a feature allowing to display Drupal's message on all pages

## [3.2.4]
 - #3314414: Added tests for 'reroute email' functionality.
 - #3318273: Make sure tests run on drupal.org infrastructure for the Domino module.
 - #3323954: Fixed issue with not working fallback for emails rerouting feature.

## [3.2.3]
- Fixed bug with installing Reroute Email module on hook_update().

## [3.2.2]
- Improved documentation & added missing changelog records

## [3.2.1]
- Issue #3313823: A lot of  "Super-admin user was successfully blocked. Username and password were updated." in the logs
- Issue #3313815: Test users do not get password updated on creation

## [3.2.0]
 - Added Reroute email module with configs which includes configuration for environments.

## [3.1.1]
- Added tests for 'automatic blocking of the super-admin user' functionality

## [3.1.0]
- Added automatic blocking of the super-admin user

## [3.0.1]
- Explicitly require Config Split v2 for schema compatibility

## [3.0.0]
- Removed legacy domino_core module.
- Removed configs which don't serve the purpose of the module
- Removed not required dependencies
- Reworked composer.json to match the desired set up
- Made Features config optional
- Updated Config Split configurations to match the latest changes on schema
- Added Drupal 10 compatibility

## [2.1.1]
- Updated dependency to PFM to use the latest major version.

## [2.1.0]
- Password and users state checking now uses cron for better performance.
- Changed timeout defaults.
- Changed README about recommended configuration.

*Upgrade path from 2.0.x to 2.1.x*:
- Enable Domino cronjob
- Upgrade `test_users_check_frequency` to be larger or equal to the cronjob frequency. Recommended value is 1 hour.
- If you don't perform actions above, the module will continue making checks on normal requests.

## [2.0.8]
- Changelog updates.

## [2.0.7]
- Password checking now uses cron for better performance.

## [2.0.6]
- Fixed warning if no active users in production.

## [2.0.5]
- Fixed warning if no active users in production.

## [2.0.4]
- Add ability to display emails instead of sending them (only for test addresses). Disabled by default.
- Add module to display sms instead of sending them.

## [2.0.3]
- No code changes - just not forgetting to write changelog records!

## [2.0.2]
- Fixed bug with wrong user email prefix & suffix.

## [2.0.1]
- Fixed bug with occasional editing of user notification settings

## [2.0.0]
- Moved logic of test users feature to the Domino module.
- Made Domino Core module obsolete (fixing old mistakes).
- Changed logic of test users:
  - Now test users are generated regardless of the application mode. Previous behavior: generate test users only on dev environment.
  - Password for test users is being set to the defined value on cache flush and also being checked regularly (each 5 mins). Previous behavior: cache flush for dev environments and each page load for production environment.
  - Renamed most variables for configuration storage for test users. See README.md for details.

## [1.0.1]
- Fixed bug with re-saving of deactivated test user accounts on each page load.
- Improved verbosity of Domino actions (added logging).
- Slightly improved performance for production users password set.
