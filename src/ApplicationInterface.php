<?php

namespace Drupal\domino;

/**
 * Application Interface.
 */
interface ApplicationInterface {

  /**
   * Application (website) mode: development.
   */
  const MODE_DEVELOPMENT = 'development';

  /**
   * Application (website) mode: staging.
   */
  const MODE_STAGING = 'staging';

  /**
   * Application (website) mode: production.
   */
  const MODE_PRODUCTION = 'production';

}
