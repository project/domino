<?php

namespace Drupal\domino;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

/**
 * Status message class.
 */
class StatusMessage {

  use StringTranslationTrait;

  /**
   * Domino config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Logger instance for Domino.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Messenger instance.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * Creates a new instance of StatusMessage.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory instance.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger instance for Domino.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, MessengerInterface $messenger) {
    $this->config = $config_factory->get('domino.settings');
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * Displays status message on all pages (if set).
   */
  public function displayStatusMessage() {
    // Don't display any messages on the production environment.
    if ($this->config->get('application_mode') === ApplicationInterface::MODE_PRODUCTION) {
      return;
    }

    // Ensure status message was set to be displayed.
    if (empty($this->config->get('status_message_display'))) {
      return;
    }

    // Ensure that message has some content defined.
    if (empty($this->config->get('status_message_content'))) {
      return;
    }

    $message_types = [
      MessengerInterface::TYPE_STATUS,
      MessengerInterface::TYPE_WARNING,
      MessengerInterface::TYPE_ERROR,
    ];

    $message_type = $this->config->get('status_message_type');
    // Fallback to the "status" message type if not defined or has incorrect
    // value.
    if (!in_array($message_type, $message_types)) {
      $message_type = MessengerInterface::TYPE_STATUS;
    }

    // Display message defined.
    $this->messenger->addMessage($this->config->get('status_message_content'), $message_type);
  }

}
