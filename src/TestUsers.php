<?php

namespace Drupal\domino;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\RoleInterface;
use Psr\Log\LoggerInterface;

/**
 * Test users class.
 */
class TestUsers {

  use StringTranslationTrait;

  /**
   * Hardcoded suffix for generated usernames of test users.
   *
   * @var string
   */
  protected string $usernameSuffix = '.test';

  /**
   * Domino config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Logger instance for Domino.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Password checker instance.
   *
   * @var \Drupal\Core\Password\PasswordInterface
   */
  protected PasswordInterface $passwordChecker;

  /**
   * Password generator instance.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected PasswordGeneratorInterface $passwordGenerator;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Creates a new instance of TestUsers.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory instance.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger instance for Domino.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager instance.
   * @param \Drupal\Core\Password\PasswordInterface $password_checker
   *   Password checker instance.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $password_generator
   *   Password generator instance.
   * @param \Drupal\Core\State\StateInterface $state
   *   State handler instance.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time object instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, PasswordInterface $password_checker, PasswordGeneratorInterface $password_generator, StateInterface $state, TimeInterface $time) {
    $this->config = $config_factory->get('domino.settings');
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->passwordChecker = $password_checker;
    $this->passwordGenerator = $password_generator;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * Makes sure test users exist and creates them if not.
   */
  public function ensureTestUsersExistence() {
    // Get domain for automatically generated user email address.
    $email_domain = $this->config->get('test_users_email_domain');
    if (empty($email_domain)) {
      $this->logger->error('Test users can not be automatically created: email domain is not set.');
      return;
    }

    // Get additional email configurations for generating test users.
    $email_prefix = $this->config->get('test_users_email_prefix') ?? '';
    $email_suffix = $this->config->get('test_users_email_suffix') ?? '';

    // For each username which does not have a corresponding user account yet
    // create it.
    $test_usernames = $this->getUsernamesWithRoles();
    foreach ($test_usernames as $username => $role_id) {
      $account = user_load_by_name($username);
      if (empty($account)) {
        // Remove username suffix ".test" from the end of username to have
        // cleaner email address.
        $suffix_length = strlen($this->usernameSuffix);
        $clean_username = substr($username, -$suffix_length) == $this->usernameSuffix ? substr($username, 0, -$suffix_length) : $username;

        // Start preparing user object for a test user.
        $account = User::create();
        $account->setUsername($username);
        $account->setEmail($email_prefix . $clean_username . $email_suffix . '@' . $email_domain);

        // Set random password for a new account, so if the user is generated
        // automatically in production or another sensitive environment then
        // nobody can log in with this account by default. We set the defined
        // password later in $this->ensureTestUsersPassword().
        $account->setPassword($this->passwordGenerator->generate());

        // Any role apart from the default authenticated role requires to be
        // added to the account.
        if ($role_id != AccountInterface::AUTHENTICATED_ROLE) {
          $account->addRole($role_id);
        }

        // Don't activate users automatically - it will be done in
        // ensureTestUsersPassword() if the conditions will allow.
        $account->block();

        // Finally, create user account in the system.
        $account->save();

        $this->logger->info($this->t('Test user @username has been created.', [
          '@username' => $account->getAccountName(),
        ]));
      }
    }
  }

  /**
   * Makes sure that test users have the expected passwords.
   */
  public function ensureTestUsersPassword() {
    // Get password used for all test users.
    $test_users_password = $this->config->get('test_users_password');

    // We should not create test users in case if password for them is not set.
    if (empty($test_users_password)) {
      $this->logger->error('Can\'t set password for test users: password is not defined in configurations.');
      return;
    }

    // Get list of all names of test users.
    $test_usernames = $this->getUsernamesWithRoles();

    // Get current application mode (i.e. "production", "development", etc).
    $application_mode = $this->config->get('application_mode');

    // Get list of test users which should not be deactivated on production
    // environment.
    $test_users_active_on_production = $this->config->get('test_users_active_on_production') ? $this->config->get('test_users_active_on_production') : [];

    // Get password for test users which should be kept active on production.
    $test_users_password_for_production = $this->config->get('test_users_password_for_production');

    // Get list of usernames of test users.
    $usernames = array_keys($test_usernames);

    if (!empty($usernames)) {
      /** @var \Drupal\user\UserInterface[] $accounts */
      $accounts = $this->entityTypeManager->getStorage('user')
        ->loadByProperties(['name' => $usernames]);

      foreach ($accounts as $account) {
        // If we're in the production website and the current account must remain
        // active for production, then make sure that its password equals
        // production password.
        if ($application_mode == ApplicationInterface::MODE_PRODUCTION
          && in_array($account->getAccountName(), $test_users_active_on_production)) {
          // Ensure production password is defined in the configs.
          if (empty($test_users_password_for_production)) {
            $this->logger->critical('Can\'t set production password for test user @username: password is not defined in configurations. User will be deactivated.', [
              '@username' => $account->getAccountName(),
            ]);
          }
          elseif (!$this->passwordChecker->check($test_users_password_for_production, $account->getPassword())) {
            $account->setPassword($test_users_password_for_production);
            $account->save();
            $this->logger->info('Production password for test user @username was successfully set to the desired value.', [
              '@username' => $account->getAccountName(),
            ]);
          }
        }
        // Otherwise, just make sure that normal password for test user is set.
        elseif (!$this->passwordChecker->check($test_users_password, $account->getPassword())) {
          $account->setPassword($test_users_password);
          $account->save();
          $this->logger->info('Password for test user @username was successfully set to the desired value.', [
            '@username' => $account->getAccountName(),
          ]);
        }
      }
    }
  }

  /**
   * Makes sure that test users are enabled or disabled.
   */
  public function ensureTestUsersActivationStatus() {
    $users_manager = $this->entityTypeManager->getStorage('user');
    // Get list of all names of test users.
    $test_usernames = $this->getUsernamesWithRoles();
    // Get only usernames of test users without roles.
    $usernames = array_keys($test_usernames);
    // Get current application mode (i.e. "production", "development", etc).
    $application_mode = $this->config->get('application_mode');

    // In the production mode we want to deactivate all test users apart
    // from the accounts which were explicitly set to remain active.
    /** @var \Drupal\user\UserInterface[] $users_to_activate */
    $users_to_activate = [];
    /** @var \Drupal\user\UserInterface[] $users_to_deactivate */
    $users_to_deactivate = [];
    if ($application_mode == ApplicationInterface::MODE_PRODUCTION) {
      $usernames_to_deactivate = [];

      // Get list of test users which should be active on production
      // environment.
      $test_users_active_on_production = $this->config->get('test_users_active_on_production') ? $this->config->get('test_users_active_on_production') : [];
      $test_users_password_for_production = $this->config->get('test_users_password_for_production');
      if (!empty($test_users_active_on_production) && !empty($test_users_password_for_production)) {
        $users_to_activate = $users_manager->loadByProperties([
          'name' => $test_users_active_on_production,
          'status' => 0,
        ]);
      }
      elseif (empty($test_users_password_for_production)) {
        // In case when production password is not defined we must deactivate
        // user even if it is set not to be deactivated on production, to
        // prevent potential security issues.
        $usernames_to_deactivate = $test_users_active_on_production;
      }

      // Each active test user account not being explicitly marked to be active
      // on production environment must be blocked.
      foreach ($usernames as $username) {
        if (!in_array($username, $test_users_active_on_production)) {
          $usernames_to_deactivate[] = $username;
        }
      }
      if (!empty($usernames_to_deactivate)) {
        $users_to_deactivate = $users_manager->loadByProperties([
          'name' => $usernames_to_deactivate,
          'status' => 1,
        ]);
      }
    }
    // If we don't operate in the production mode, then all test users
    // must be enabled.
    elseif (!empty($usernames)) {
      // Load only accounts which are blocked and must be activated.
      /** @var \Drupal\user\UserInterface[] $users_to_activate */
      $users_to_activate = $users_manager->loadByProperties([
        'name' => $usernames,
        'status' => 0,
      ]);
    }

    if (!empty($users_to_activate) || !empty($users_to_deactivate)) {
      // Temporary modify user notification settings to make sure that
      // the system will not be sending emails to test users on activation
      // or deactivation.
      $user_settings = $this->configFactory->getEditable('user.settings');
      $status_activated = $user_settings->get('notify.status_activated');
      $status_blocked = $user_settings->get('notify.status_blocked');
      $user_settings->set('notify.status_activated', FALSE);
      $user_settings->set('notify.status_blocked', FALSE);
      $user_settings->save();

      foreach ($users_to_activate as $account) {
        $account->activate();
        $account->save();
        $this->logger->info('Test user @username was successfully activated.', [
          '@username' => $account->getAccountName(),
        ]);
      }

      foreach ($users_to_deactivate as $account) {
        $account->block();
        $account->save();
        $this->logger->info('Test user @username was successfully blocked.', [
          '@username' => $account->getAccountName(),
        ]);
      }

      // Restore user notification settings.
      $user_settings->set('notify.status_activated', $status_activated);
      $user_settings->set('notify.status_blocked', $status_blocked);
      $user_settings->save();
    }
  }

  /**
   * Returns list of usernames and their related roles for test users.
   *
   * @return array
   *   List of usernames and their related roles.
   */
  public function getUsernamesWithRoles(): array {
    $test_users = [];

    $roles = Role::loadMultiple();
    foreach ($roles as $role) {
      if ($role->id() != RoleInterface::ANONYMOUS_ID) {
        $username = $this->getUsernameByRole($role->id());
        $test_users[$username] = $role->id();
      }
    }

    // Add extra users from config if they're defined.
    $additional_users = $this->config->get('test_users_additional_users');
    if (!empty($additional_users)) {
      foreach ($additional_users as $username => $role_id) {
        $test_users[$username] = $role_id;
      }
    }

    return $test_users;
  }

  /**
   * Generates username by role machine name.
   *
   * @param string $role_machine_name
   *   Role's machine name.
   *
   * @return string
   *   Username generated for the given role name.
   */
  public function getUsernameByRole(string $role_machine_name): string {
    // Get mapping of test usernames from config in case if the username
    // should be different from the default value.
    $test_usernames_map = $this->config->get('test_users_usernames_map');

    // Map test user's name according to the config or just user role's machine
    // name.
    if (!empty($test_usernames_map[$role_machine_name])) {
      $username = $test_usernames_map[$role_machine_name];
    }
    else {
      $username = $role_machine_name;
    }

    return $username . '.test';
  }

  /**
   * Checking actual status of test users in the system and passwords.
   *
   * Could be running as fallback variant if cron job from domino.module didn't run.
   *
   * @param string $source
   *   Could be "cron" or "fallback".
   */
  public function regularCheck(string $source = 'cron') {
    // Regularly ensure that test users are in the actual activation status
    // and their password is correct.
    $last_check = $this->state->get('domino.last_test_users_check', 0);
    $current_time = $this->time->getCurrentTime();
    // Defaults to 1 hour. You can set it up in domino.settings.yml: test_users_check_frequency.
    $check_frequency = $this->config->get('test_users_check_frequency');
    if ($check_frequency === NULL) {
      $this->logger->error('Users checking frequency is not set.');
      if ($source === 'fallback') {
        return;
      }
    }
    // We use multiplier to prevent running fallback if cron fails several times.
    $multiplier = 3;
    if ($source === 'cron') {
      $multiplier = 1;
    }
    $need_to_check = ($last_check + $check_frequency * $multiplier) < $current_time;
    if ($need_to_check) {
      if ($source === 'fallback') {
        $this->logger->warning('Running fallback for checking test users. Cron frequency should be configured according to test_users_check_frequency setting.');
      }
      // The point is that in case of someone occasionally enables test user or changes
      // their password - domino reverts them back to the desired state.
      $this->ensureTestUsersActivationStatus();
      $this->ensureTestUsersPassword();
      $this->state->set('domino.last_test_users_check', $current_time);
      $this->logger->info('Users checking was successfully finished.');
    }
  }

}
