<?php

namespace Drupal\domino;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Hooks into each page request and displays status message if configured.
 */
class StatusMessageSubscriber implements EventSubscriberInterface {

  /**
   * Status message handler.
   *
   * @var \Drupal\domino\StatusMessage
   */
  protected StatusMessage $statusMessage;

  /**
   * Constructs a new StatusMessageSubscriber instance.
   *
   * @param \Drupal\domino\StatusMessage $status_message
   *   Status message object instance.
   */
  public function __construct(StatusMessage $status_message) {
    $this->statusMessage = $status_message;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['displayStatusMessage'];
    return $events;
  }

  /**
   * Displays status message on each page (if defined).
   */
  public function displayStatusMessage() {
    $this->statusMessage->displayStatusMessage();
  }

}
