<?php

namespace Drupal\domino;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

/**
 * Super-admin user class.
 */
class SuperAdminUser {

  use StringTranslationTrait;

  /**
   * Domino config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Config factory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Logger instance for Domino.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Password generator instance.
   *
   * @var \Drupal\Core\Password\PasswordGeneratorInterface
   */
  protected PasswordGeneratorInterface $passwordGenerator;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Creates a new instance of TestUsers.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory instance.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger instance for Domino.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager instance.
   * @param \Drupal\Core\Password\PasswordGeneratorInterface $password_generator
   *   Password generator instance.
   * @param \Drupal\Core\State\StateInterface $state
   *   State handler instance.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time object instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, PasswordGeneratorInterface $password_generator, StateInterface $state, TimeInterface $time) {
    $this->config = $config_factory->get('domino.settings');
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->passwordGenerator = $password_generator;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * Creates super admin user if not exists yet.
   *
   * @return void
   */
  public function ensureSuperAdminUserExistence() {
    try {
      $user_storage = $this->entityTypeManager->getStorage('user');
      $anonymous_user = $user_storage->load(1);
      if (empty($anonymous_user)) {
        $user_storage->create([
          'uid' => 1,
          'name' => 'placeholder-for-uid-1',
          'mail' => 'placeholder-for-uid-1',
          'status' => 0,
        ])
          ->save();
        $this->logger->info('Domino has created super-admin user in the database.');
      }
    } catch (\Exception $exception) {
      $this->logger->error('Domino could not create super-admin user. Error: @error', [
        '@error' => $exception->getMessage()
      ]);
    }
  }

  /**
   * Makes sure that super-admin user is secure.
   */
  public function ensureSuperAdminUserIsSecure() {
    /** @var \Drupal\user\UserInterface $super_admin_account */
    $super_admin_account = $this->entityTypeManager->getStorage('user')->load(1);

    // There are cases when uid: 1 may not exist. In this case there's nothing
    // to do here.
    if (empty($super_admin_account)) {
      return;
    }

    $update_required = FALSE;
    if ($super_admin_account->isActive()) {
      $this->logger->error('Super-admin user @username is active.', [
        '@username' => $super_admin_account->getAccountName(),
      ]);
      $update_required = TRUE;
    }

    // Ensure that super-admin username is randomized.
    if (!str_starts_with($super_admin_account->getAccountName(), 'superadmin.blocked.')) {
      $update_required = TRUE;
    }

    if ($update_required) {
      // Temporary modify user notification settings to make sure that
      // the system will not be sending emails to super-admin user on deactivation.
      $user_settings = $this->configFactory->getEditable('user.settings');
      $status_activated = $user_settings->get('notify.status_activated');
      $status_blocked = $user_settings->get('notify.status_blocked');
      $user_settings->set('notify.status_activated', FALSE);
      $user_settings->set('notify.status_blocked', FALSE);
      $user_settings->save();

      $super_admin_account->setUsername('superadmin.blocked.' . $this->passwordGenerator->generate(8));
      $super_admin_account->setPassword($this->passwordGenerator->generate(16));
      $super_admin_account->block();
      $super_admin_account->save();

      $this->logger->info('Super-admin user was successfully blocked. Username and password were updated.');

      // Restore user notification settings.
      $user_settings->set('notify.status_activated', $status_activated);
      $user_settings->set('notify.status_blocked', $status_blocked);
      $user_settings->save();
    }
  }

  /**
   * Checking actual status of super-admin user in the system.
   *
   * Could be running as fallback variant if cron job from domino.module didn't run.
   *
   * @param string $source
   *   Could be "cron" or "fallback".
   */
  public function regularCheck(string $source = 'cron') {
    // Regularly ensure that super-admin user is secure.
    $last_check = $this->state->get('domino.last_super_admin_user_check', 0);
    $current_time = $this->time->getCurrentTime();
    // Defaults to 1 hour. You can set it up in domino.settings.yml: super_admin_user_check_frequency.
    $check_frequency = $this->config->get('super_admin_user_check_frequency');
    if ($check_frequency === NULL) {
      $this->logger->error('Super-admin user checking frequency is not set.');
      if ($source === 'fallback') {
        return;
      }
    }
    // We use multiplier to prevent running fallback if cron fails several times.
    $multiplier = 3;
    if ($source === 'cron') {
      $multiplier = 1;
    }
    $need_to_check = ($last_check + $check_frequency * $multiplier) < $current_time;
    if ($need_to_check) {
      if ($source === 'fallback') {
        $this->logger->warning('Running fallback for checking super-admin user. Cron frequency should be configured according to super_admin_user_check_frequency setting.');
      }
      // The point is that in case of someone occasionally enables super-admin user
      // or changes their password - domino reverts them back to the desired state.
      $this->ensureSuperAdminUserIsSecure();
      $this->state->set('domino.last_super_admin_user_check', $current_time);
      $this->logger->info('Super-admin user checking was successfully finished.');
    }
  }

}
