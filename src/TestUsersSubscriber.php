<?php

namespace Drupal\domino;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Ensures actual status of test users in the system if cron is not running properly.
 */
class TestUsersSubscriber implements EventSubscriberInterface {

  /**
   * Test users handler.
   *
   * @var \Drupal\domino\TestUsers
   */
  protected TestUsers $testUsers;

  /**
   * Constructs a new TestUsersSubscriber instance.
   *
   * @param \Drupal\domino\TestUsers $test_users
   *   Test users object instance.
   */
  public function __construct(TestUsers $test_users) {
    $this->testUsers = $test_users;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['ensureTestUsersStatus'];
    return $events;
  }

  /**
   * Ensure actual status of test users in the system.
   *
   * Running as fallback variant if cron job from domino.module didn't run.
   *
   * Makes sure that test users exist, contain actual
   * password and are activated or deactivated.
   */
  public function ensureTestUsersStatus() {
    $this->testUsers->regularCheck('fallback');
  }

}
