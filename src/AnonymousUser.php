<?php

namespace Drupal\domino;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;

/**
 * Anonymous user class.
 */
class AnonymousUser {

  use StringTranslationTrait;

  /**
   * Logger instance for Domino.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Creates a new instance of AnonymousUser.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger instance for Domino.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager instance.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Creates anonymous user if not exists yet.
   *
   * @return void
   */
  public function ensureAnonymousUserExistence() {
    try {
      $user_storage = $this->entityTypeManager->getStorage('user');
      $anonymous_user = $user_storage->load(0);
      if (empty($anonymous_user)) {
        $user_storage->create([
          'uid' => 0,
          'status' => 0,
          'name' => '',
        ])
          ->save();
        $this->logger->info('Domino has created anonymous user in the database.');
      }
    } catch (\Exception $exception) {
      $this->logger->error('Domino could not create anonymous user. Error: @error', [
        '@error' => $exception->getMessage()
      ]);
    }
  }

}
