<?php

namespace Drupal\domino;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Ensures super-admin user is secure in the system if cron is not running properly.
 */
class SuperAdminUserSubscriber implements EventSubscriberInterface {

  /**
   * Super-admin user handler.
   *
   * @var \Drupal\domino\SuperAdminUser
   */
  protected SuperAdminUser $superAdminUser;

  /**
   * Constructs a new SuperAdminUserSubscriber instance.
   *
   * @param \Drupal\domino\SuperAdminUser $super_admin_user
   *   Super-admin user object instance.
   */
  public function __construct(SuperAdminUser $super_admin_user) {
    $this->superAdminUser = $super_admin_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['ensureSuperAdminUserSecure'];
    return $events;
  }

  /**
   * Ensure super-admin user is secure in the system.
   *
   * Running as fallback variant if cron job from domino.module didn't run.
   *
   * Makes sure that super-admin user is deactivated and contain secure
   * password and username.
   */
  public function ensureSuperAdminUserSecure() {
    $this->superAdminUser->regularCheck('fallback');
  }

}
