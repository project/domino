Domino
======

The module focuses on providing helpful features for developers focused on ease of development, automated tests &
enhanced security for 3 types of environments: production, staging and development.

The module is designed for use by developers and does not have admin UI for configuration. All settings are supposed
to be set in `settings.*.php` files for different project environments.

Dependencies
------------

- [REQUIRED] [Configuration Split](https://www.drupal.org/project/config_split)
- [REQUIRED] [Reroute email](https://www.drupal.org/project/reroute_email)
- [OPTIONAL] [Features](https://www.drupal.org/project/features) - to receive the latest config updates from Domino

Configuration
-------------

Firstly, you need to let Domino know which environment is currently active, so that it can adjust the behavior of
its features accordingly. To do it you need to set the application mode in the appropriate `settings.php` file for
each environment:

```
// Allowed values: 'development' || 'staging' || 'production'.
// Default: 'production'.
$config['domino.settings']['application_mode'] = 'production'
```

All other configurations are feature-specific, and you'll find their description below. You can find list of all
possible settings in `./config/install/domino.settings.yml`.

Here's an example of standard Domino configuration for a project:

```
// Configure the Domino module.
$config['domino.settings'] = [
  // Application mode can be "development", "staging" or "production".
  'application_mode' => getenv('APPLICATION_MODE'),
  // Required variable to work with test users.
  'test_users_password' => getenv('TEST_USERS_PASSWORD'),
  // Generates emails for test users like "dev+$username@systemseed.com".
  'test_users_email_domain' => 'systemseed.com',
  'test_users_email_prefix' => 'dev+',
];
```

Features
--------

* [Default user roles](#user-roles)
* [Configuration splits](#configuration-splits)
* [Emails rerouting for development environments](#reroute-email)
* [Blocking super-admin](#blocking-super-admin)
* [Tests users management for development and production environments](#test-users)
* [Test email delivery using MailSlurp](#test-email-delivery-using-mailslurp)
* [Display emails as Drupal messages](#display-emails-as-drupal-messages)
* [Display SMS as Drupal messages](#display-sms-as-drupal-messages)
* [Display message on all pages](#display-message-on-all-pages)
* [Readiness for truncated user tables](#readiness-for-truncated-user-tables)

#### User roles

Domino module installs  2 user roles: Developer and Manager.

- Developer role is assumed for someone with full access to admin the whole system (usually developers)
- Manager role is assumed for people with full access to the content & users (top privileges for non-developer)

Note that the permissions for the Manager role have to be configured manually. Domino only standardizes commonly used
role names across projects.

#### Configuration splits

Domino module comes with 3 configuration splits, 1 for each environment:

* Development
* Staging
* Production

To start using config splits:

1. In your configuration directory (by default - on the same level as the docroot) create "split" folder with 3 children:

```
config
--split
----development
----production
----staging
--sync
```

2. Set path to split folders in `settings.php` files (note that in your case path to config folder may be different):

```
$config['config_split.config_split.development']['storage'] = 'folder';
$config['config_split.config_split.development']['folder'] = '../config/split/development';
$config['config_split.config_split.staging']['storage'] = 'folder';
$config['config_split.config_split.staging']['folder'] = '../config/split/staging';
$config['config_split.config_split.production']['storage'] = 'folder';
$config['config_split.config_split.production']['folder'] = '../config/split/production';
```

3. Activate split for each environment in their own `settings.*.php` file:

```
$config['config_split.config_split.development']['status'] = TRUE;
$config['config_split.config_split.staging']['status'] = TRUE;
$config['config_split.config_split.production']['status'] = TRUE;
```

Pay attention that in the config above we enable all splits, but on your project you need to enable only one of these
for each environment.

#### Reroute email

Domino comes with email rerouting functionality for development environments, so that emails from such environments
are never sent to real users. By default, Reroute Email module is enabled but the feature is disabled. It's expected
that developers will enable rerouting configuration for their development environments in `settings.*.php` files.

If Reroute Email is not enabled for non-production environments, Domino will prevent all emails from being sent until
rerouting is configured.

Example of Reroute Email configuration for development environment in its `settings.*.php` file:

```
$config['reroute_email.settings']['enable']  = TRUE;
$config['reroute_email.settings']['address'] = 'test@systemseed.com';
$config['reroute_email.settings']['allowed'] = '*@systemseed.com';
```

### Blocking super-admin

Domino automatically blocks super-admin user (uid = 1) and changes their username and password. This is done for
security reasons - they have too many permissions which can't be removed. It makes this user a perfect candidate for
hacking attacks. The module deactivates super-admin and changes name / password on cron execution or cache flush.

### Test users

Domino handles creation and enabling / disabling test users. By default, the module creates 1 test user for each role in
the system (apart from anonymous role) on cache flush. These users are enabled on non-production environments and
disabled on production environment. This is an example of minimal configuration to be set in `settings.php` file:

```
$config['domino.settings'] = [
  // Note that TEST_USERS_PASSWORD variable has to exist in your php environment.
  'test_users_password' => getenv('TEST_USERS_PASSWORD'),
  'test_users_email_domain' => 'systemseed.com',
];
```

Value of `test_users_password` setting will be used as a password for all test users. Pattern for test users username
is: `ROLE.test`. It is possible to use an alias for `ROLE` for better naming:

```
$config['domino.settings']['test_users_usernames_map'] = [
  'administrator' => 'admin',
];
```

Note: `test_users_usernames_map` accepts arrays of user role machine names and expected alias.

It is possible to define a list of users who shouldn't be disabled on production via setting
`test_users_active_on_production`. In this case you also have to set `test_users_password_for_production` variable
with super-secure password value.

```
// TEST_USERS_PASSWORD_FOR_PRODUCTION should be already defined in your environment.
$config['domino.settings'][test_users_password_for_production'] = getenv('TEST_USERS_PASSWORD_FOR_PRODUCTION');

// List test user names to keep active on production.
$config['domino.settings']['test_users_active_on_production'] = [
  'manager.test',
];
```

There is a possibility to add more test users:

```
// It's a mapping of user names and role IDs to assign to them.
$config['domino.settings']['test_users_additional_users'] = [
  'one_more_developer_user' => 'developer',
  'another_developer_user' => 'developer',
];
```

### Test email delivery using MailSlurp

Domino allows you to test email delivery using MailSlurp service. In order to use it, first create a MailSlurp account.
Create a permanent inbox in the MailSlurp dashboard. Then add the inbox id and api key provided by the service
and enable the feature in `settings.php` file:
```yaml
// Reroute all emails to a MailSlurp inbox specified by a cookie.
$config['domino.settings']['test_email_delivery'] = TRUE;
$config['domino.settings']['mailslurp_key'] = getenv("MAILSLURP_KEY");
$config['domino.settings']['mailslurp_inbox_id'] = getenv("MAILSLURP_INBOX_ID");
```

In your acceptance tests set `domino_test_email_inbox` cookie. For example:
```php
use MailSlurp\Apis\InboxControllerApi;
use MailSlurp\Apis\WaitForControllerApi;
use MailSlurp\Configuration;
use MailSlurp\Models\Email;

// ...

// Initiate API.
$apiKey = getenv('MAILSLURP_KEY');
$I->assertNotEmpty($apiKey, "MAILSLURP_KEY environmental variable should be set to test email delivery.");
$config = Configuration::getDefaultConfiguration()->setApiKey('x-api-key', $apiKey);

// A. Generate email on the go.
$inboxController = new InboxControllerApi(null, $config);
$inboxId = $inboxController->createInbox()->getId();
$I->assertNotEmpty($inboxId, "MAILSLURP_INBOX_ID environmental variable should be set to test email delivery.");
$I->setCookie('domino_test_email_inbox', $inboxId);

// B. Submit contact the form as usual using the pre-created MailSlurp inbox email.
$inboxId = getenv('MAILSLURP_INBOX_ID');
$I->assertNotEmpty($inboxId, "MAILSLURP_INBOX_ID environmental variable should be set to test email delivery.");

// Wait for the mail.
$waitForController = new WaitForControllerApi(null, $config);
$email = $waitForController->waitForLatestEmail($inboxId, self::MAILSLURP_TIMEOUT, true);
```

### Display emails as Drupal messages

This feature is particularly useful for writing automated tests. When it's activated, then on each email sending
to any test user in the system, instead of actually sending the email, its content is displayed as a Drupal message,
so that the content can be verified as well as links can be clicked / obtained for further testing.

To enable this feature, turn on this setting:

```
$config['domino.settings']['display_emails_as_messages'] = TRUE;
```

Note that this does not work when `application_mode` is set to `production`.

### Display SMS as Drupal messages

This feature works same as above but for sending sms. It requires enabling a submodule "domino_sms". To activate the
feature:

```
$config['domino_sms.settings']['display_sms_as_messages'] = TRUE;
```

### Display message on all pages

Domino has a feature which can display a standard Drupal message on all pages. This is particularly helpful to remind
developers about expected next steps or display a message with a warning for something.

To enable the feature, set the following configs:

```
$config['domino_sms.settings']['status_message_display'] = TRUE;
$config['domino_sms.settings']['status_message_content'] = 'This is a development environment';
// Allowed types: 'status', 'warning', 'error'.
$config['domino_sms.settings']['status_message_type'] = 'warning";
```

Note that the message won't be displayed on production environments.

### Readiness for truncated user tables

In professional secure environment data obfuscation or truncation is a must, to ensure that PII (personal identifiable
information) is not present on developers machines. Such solutions may include truncation of all user data (i.e. when
database backup is being exported without user tables). This leads to missing anonymous and super-admin users, or broken
references to non-existing users in Drupal, resulting in random errors on Drupal side.

Therefore, Domino makes sure that anonymous and super-admin users are always present in the database. As well as that,
it has a graceful fallback for all entities with non-existing owners to set to anonymous user.
