<?php

namespace Drupal\Tests\domino\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\domino\ApplicationInterface;
use Drupal\Core\Test\AssertMailTrait;

/**
 * Tests Reroute email functionality for all environments.
 *
 * @group domino
 */
class RerouteEmailTest extends KernelTestBase
{
  use AssertMailTrait {
    getMails as drupalGetMails;
  }

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'domino',
    'reroute_email',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp()
  {
    parent::setUp();

    $this->installEntitySchema('user');
  }

  /**
   * Run tests for Development environment.
   */
  public function testDevelopment() {
    $this->runTestsForEnvironment(ApplicationInterface::MODE_DEVELOPMENT);
  }

  /**
   * Run tests for Staging environment.
   */
  public function testStaging() {
    $this->runTestsForEnvironment(ApplicationInterface::MODE_STAGING);
  }

  /**
   * Run tests for Production environment.
   */
  public function testProduction() {
    $this->runTestsForEnvironment(ApplicationInterface::MODE_PRODUCTION);
  }

  /**
   * Run all tests for the certain environment.
   *
   * @param string $env
   *   The name of environment.
   */
  private function runTestsForEnvironment($env) {
    $this->setCurrentEnvironment($env);

    // Test the case when "rerouting" is enabled, the email was sent.
    $this->setReroutingEnableProperty(TRUE);
    $this->sendTestEmail();
    $emails = $this->drupalGetMails();
    $this->assertCount(1, $emails);
    $this->assertNotNull($emails[0]['headers']['X-Rerouted-Status']);
    $this->assertSame('REROUTED', $emails[0]['headers']['X-Rerouted-Status']);
    $this->assertSame('to@example.com', $emails[0]['headers']['X-Rerouted-Original-to']);
    $this->assertSame('rerouted-to@example.com', $emails[0]['to']);
    $this->assertTrue($emails[0]['send']);

    // Test the case when "rerouting" is disabled, the email wasn't sent for all
    // environment apart from production.
    $this->setReroutingEnableProperty(FALSE);
    $this->sendTestEmail();
    $emails = $this->drupalGetMails();
    $this->assertCount($env === ApplicationInterface::MODE_PRODUCTION ? 2 : 1, $emails);

    // Extra tests for production environment.
    if ($env === ApplicationInterface::MODE_PRODUCTION) {
      $this->assertFalse(in_array('X-Rerouted-Status', $emails[1]['headers']));
      $this->assertSame('to@example.com', $emails[1]['to']);
    }
  }

  /**
   * Set the current environment and make sure that it was applied.
   *
   * @param string $env
   *   The name of environment.
   */
  private function setCurrentEnvironment($env) {
    $configs = \Drupal::configFactory()->getEditable('domino.settings');
    $configs->set('application_mode', $env)->save();

    $this->assertSame($configs->get('application_mode'), $env);
  }

  /**
   * Set "enabled" property for rerouting email.
   *
   * @param bool $enabled
   *   Indicates whether the rerouting is enabled.
   */
  private function setReroutingEnableProperty($enabled) {
    $configs = \Drupal::configFactory()->getEditable('reroute_email.settings');

    // Set default properties.
    $configs->set('allowed', '')->save();
    $configs->set('mailkeys', '')->save();
    $configs->set('mailkeys_skip', '')->save();
    $configs->set('address', 'rerouted-to@example.com')->save();

    // Set enable "property".
    $configs->set('enable', $enabled)->save();

    $this->assertSame($configs->get('enable'), $enabled);
  }

  /**
   * Send an email.
   */
  private function sendTestEmail() {
    /** @var \Drupal\Core\Mail\MailManagerInterface $mailManager */
    $mailManager = \Drupal::service('plugin.manager.mail');

    // Prepare params.
    $params = [
      'subject' => $this->randomString(64),
      'body' => $this->randomString(128),
    ];

    $mailManager->mail('domino', 'test_mail', 'to@example.com', 'en', $params);
  }

}
