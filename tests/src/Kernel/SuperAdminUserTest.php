<?php

namespace Drupal\Tests\devel\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests blocking super-admin user functionality.
 *
 * @group domino
 */
class SuperAdminUserTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * Set to TRUE to strict check all configuration saved.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Modules to enable.
   *
   * We don't enable Domino module by default to be able to test installation
   * behaviour.
   *
   * @var array
   */
  public static $modules = ['system', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(['system']);
    $this->installEntitySchema('user');

  }

  /**
   * Check super-admin user after installation of Domino module.
   *
   * Super-admin user should be updated and blocked during installation
   * of Domino module.
   */
  public function testSuperAdminInstall() {
    // Create a super-admin user.
    $this->createUser([], 'admin', TRUE);
    /** @var \Drupal\user\Entity\User $user */
    $user = \Drupal::entityTypeManager()->getStorage('user')->load(1);

    $this->assertEquals('admin', $user->getAccountName(), "Super-admin user has an account name different from 'admin'. Expected: super-admin has 'admin' account name.");
    $this->assertTrue($user->isActive(), "Super-admin user isn't active. Expected: super-admin user is active.");

    // Install Domino and make sure super-admin user is changed and blocked.
    \Drupal::service('module_installer')->install(['domino']);

    /** @var \Drupal\user\Entity\User $user */
    $user = \Drupal::entityTypeManager()->getStorage('user')->load(1);

    $this->assertStringContainsString('superadmin.blocked.', $user->getAccountName(), "Super-admin account name doesn't contain 'superadmin.blocked.'.");
    $this->assertFalse($user->isActive(), "Super-admin user is active. Expected: super-admin user is blocked.");
  }

  /**
   * Check super-admin after cache flush.
   *
   * Super-admin user should be updated and blocked during cache flash.
   */
  public function testSuperAdminCacheFlush() {
    // Create a super-admin user.
    $this->createUser([], 'admin', TRUE);
    \Drupal::service('module_installer')->install(['domino']);

    // Super-admin user will be blocked during installation of Domino module.
    // We need to revert status and username back for further tests.
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');
    /** @var \Drupal\user\Entity\User $user */
    $user = $user_storage->load(1);
    $user->activate();
    $user->setUsername('admin');
    $user->save();

    // Make sure the super-admin user is enabled and has 'admin' username.
    $this->assertEquals('admin', $user->getAccountName(), "Super-admin user has an account name different from 'admin'. Expected: super-admin has 'admin' account name.");
    $this->assertTrue($user->isActive(), "Super-admin user isn't active. Expected: super-admin user is active.");

    // Flush drupal cache and make sure super-admin user is changed and blocked.
    drupal_flush_all_caches();

    /** @var \Drupal\user\Entity\User $user */
    $user = $user_storage->load(1);

    $this->assertStringContainsString('superadmin.blocked.', $user->getAccountName(), "Super-admin account name doesn't contain 'superadmin.blocked.'.");
    $this->assertFalse($user->isActive(), "Super-admin user is active. Expected: super-admin user is blocked.");
  }

  /**
   * Check super-admin after cron happened.
   *
   * Super-admin user should be updated and blocked by cron.
   */
  public function testSuperAdminCron() {
    // Create a super-admin user.
    $this->createUser([], 'admin', TRUE);
    \Drupal::service('module_installer')->install(['domino']);

    // Super-admin user will be blocked during installation of Domino module.
    // We need to revert status and username back for further tests.
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');
    /** @var \Drupal\user\Entity\User $user */
    $user = $user_storage->load(1);
    $user->activate();
    $user->setUsername('admin');
    $user->save();

    // Make sure the super-admin user is enabled and has 'admin' username.
    $this->assertEquals('admin', $user->getAccountName(), "Super-admin user has an account name different from 'admin'. Expected: super-admin has 'admin' account name.");
    $this->assertTrue($user->isActive(), "Super-admin user isn't active. Expected: super-admin user is active.");

    // Run drupal cron and make sure super-admin user is changed and blocked.
    \Drupal::service('cron')->run();

    /** @var \Drupal\user\Entity\User $user */
    $user = $user_storage->load(1);
    $this->assertStringContainsString('superadmin.blocked.', $user->getAccountName(), "Super-admin account name doesn't contain 'superadmin.blocked.'.");
    $this->assertFalse($user->isActive(), "Super-admin user is active. Expected: super-admin user is blocked.");
  }

}
