<?php

namespace Drupal\domino_sms\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\domino\ApplicationInterface;
use Drupal\sms\Event\SmsEvents;
use Drupal\sms\Event\SmsMessageEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Domino SMS event subscriber.
 */
class DominoSmsSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(MessengerInterface $messenger, ConfigFactoryInterface $configFactory) {
    $this->messenger = $messenger;
    $this->configFactory = $configFactory;
  }

  /**
   * Event handler.
   *
   * @param \Drupal\sms\Event\SmsMessageEvent $event
   *   Event.
   */
  public function onMessagePreprocess(SmsMessageEvent $event) {
    $dominoConfig = $this->configFactory->get('domino.settings');
    $config = $this->configFactory->get('domino_sms.settings');
    if ($dominoConfig->get('application_mode') === ApplicationInterface::MODE_PRODUCTION || !$config->get('display_sms_as_messages')) {
      return;
    }
    foreach ($event->getMessages() as $message) {
      $text = $message->getMessage();
      // Wrap any 6 digit code with markup.
      $text = preg_replace('/\d{6}/', '<span class="otp-code">$0</span>', $text);
      $textWithMarkup = Markup::create($text);
      $this->messenger->addStatus($textWithMarkup);
    }
    // Avoid sending the messages.
    $event->setMessages([]);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SmsEvents::MESSAGE_PRE_PROCESS => ['onMessagePreprocess'],
    ];
  }

}
